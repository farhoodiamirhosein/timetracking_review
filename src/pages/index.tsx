import BackgroundWrapper from '@/hocs/backgroundWrapper'
import TrackerWrapper from '@/hocs/trackerWrapper'
import { getTimeTracking } from '@/api/timeTracking'
import { GetTimeTrackingResponseTypeProps } from '@/api/timeTracking/requestTypes'
import TimeTrackingProvider from '@/contexts/timeTrackerContext'
import TimeTrackingComponent from '@/components/pages/main'
import { showTypes } from '@/components/pages/main/sidebar/typeSwitcher'
import Head from 'next/head'
import { GetServerSidePropsContext } from 'next'

export default function Home({
  data,
}: {
  data: GetTimeTrackingResponseTypeProps
}) {
  return (
    <>
      <Head>
        <title>Time Tracking App</title>
      </Head>
      <BackgroundWrapper>
        <TrackerWrapper>
          <TimeTrackingProvider
            initialTimeTrakings={{ data, type: showTypes[1] }}
          >
            <TimeTrackingComponent />
          </TimeTrackingProvider>
        </TrackerWrapper>
      </BackgroundWrapper>
    </>
  )
}

export const getServerSideProps = async ({
  res,
}: GetServerSidePropsContext) => {
  try {
    const res = await getTimeTracking()
    return {
      props: {
        data: res.data,
      },
    }
  } catch (e) {
    return {
      notFound: true,
    }
  }
}
