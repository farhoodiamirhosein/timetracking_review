import type { AppProps } from 'next/app'
import '@/styles/globals.css'
import { Rubik } from 'next/font/google'

const rubik = Rubik({
  subsets: ['latin'],
  variable: '--rubik-font',
  weight: ['300', '400', '500', '600'],
})

export default function App({ Component, pageProps }: AppProps) {
  return (
    <main className={`${rubik.variable} font-rubik`}>
      <Component {...pageProps} />
    </main>
  )
}
