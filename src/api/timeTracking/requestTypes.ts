export type ResponseType<T> = {
  data: T
}

export type TimeFrameTitleProps =
  | 'Play'
  | 'Study'
  | 'Work'
  | 'Exercise'
  | 'Social'
  | 'Self Care'

export type TimeFrameTypeProps = {
  daily: {
    current: number
    previous: number
  }
  monthly: {
    current: number
    previous: number
  }
  weekly: {
    current: number
    previous: number
  }
}

export type GetUserResponseTypeProps = {
  name: string
  img: string
}

export type TimeTrackingStateTypeProps = {
  title: TimeFrameTitleProps
  timeframes: TimeFrameTypeProps
}

export type GetTimeTrackingResponseTypeProps = TimeTrackingStateTypeProps[]
