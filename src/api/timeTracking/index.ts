import axioIstance from '../axios'
import timeTrackingEndPoints from './endPoints'
import { GetTimeTrackingResponseTypeProps, ResponseType } from './requestTypes'

export const getTimeTracking = async () => {
  const res: ResponseType<GetTimeTrackingResponseTypeProps> =
    await axioIstance.get(timeTrackingEndPoints.default)
  return res
}
