const baseEndPoint = '/time-tracking'

const timeTrackingEndPoints = {
  default: `${baseEndPoint}`,
}

export default timeTrackingEndPoints
