import React from 'react'
import IcomoonReact from 'icomoon-react'
import iconSet from './selection.json'

// NOTE: for adding icon you must go to https://icomoon.io/app/#/select import current selection.json from ./ and update it and import it again

export interface IconTypeProps {
  name: string
  size?: number
  color?: string
  className?: string
  onClick?: any
  style?: React.CSSProperties
  onMouseEnter?: () => void
  onMouseLeave?: () => void
}

const Icon = ({
  size = 40,
  color,
  className,
  onClick,
  style,
  name,
  onMouseEnter,
  onMouseLeave,
}: IconTypeProps) => {
  return (
    <span
      onClick={onClick}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
    >
      <IcomoonReact
        iconSet={iconSet}
        color={color}
        size={size}
        icon={'icon-' + name}
        className={className}
        style={style}
      />
    </span>
  )
}

export default Icon
