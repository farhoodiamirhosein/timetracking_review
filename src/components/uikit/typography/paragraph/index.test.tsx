import React from 'react'
import { render, screen } from '@testing-library/react'
import Paragraph from './index'

describe('Paragraph', () => {
  test('renders the children', () => {
    const text = 'Hello, world!'
    render(<Paragraph>{text}</Paragraph>)
    const paragraphElement = screen.getByText(text)
    expect(paragraphElement).toBeInTheDocument()
  })

  test('applies the className prop', () => {
    const className = 'custom-class'
    render(<Paragraph className={className}>Text</Paragraph>)
    const paragraphElement = screen.getByText('Text')
    expect(paragraphElement).toHaveClass(className)
  })
})
