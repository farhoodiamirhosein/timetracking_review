import classnames from 'classnames'

const Paragraph = (props: {
  children: string | number
  className?: string
}) => {
  const { children, className } = props
  return (
    <p className={className} {...props}>
      {children}
    </p>
  )
}
export default Paragraph
