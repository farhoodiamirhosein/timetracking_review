import { render, screen } from '@testing-library/react'
import Heading from './index'

describe('Heading component', () => {
  it('renders with the correct text and tag', () => {
    const { getByText } = render(<Heading type="h2">Hello, world!</Heading>)
    const headingElement = getByText(/Hello, world!/i)
    expect(headingElement.tagName).toBe('H2')
  })
  it('applies the className prop', () => {
    const className = 'custom-class'
    render(<Heading className={className}>Text</Heading>)
    const paragraphElement = screen.getByText('Text')
    expect(paragraphElement).toHaveClass(className)
  })
})
