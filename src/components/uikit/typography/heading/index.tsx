import classnames from 'classnames'

const Heading = (props: {
  children: string
  className?: string
  type?: 'h1' | 'h2' | 'h3'
}) => {
  const { children, className, type = 'h1' } = props
  const renderByType = () => {
    if (type === 'h3')
      return (
        <h3 className={className} {...props}>
          {' '}
          {children}{' '}
        </h3>
      )
    if (type === 'h2')
      return (
        <h2 className={className} {...props}>
          {' '}
          {children}{' '}
        </h2>
      )
    return (
      <h1 className={className} {...props}>
        {' '}
        {children}{' '}
      </h1>
    )
  }

  return renderByType()
}
export default Heading
