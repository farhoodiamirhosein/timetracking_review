import { render, fireEvent } from '@testing-library/react'
import Ellipsis from './index'

describe('Ellipsis component', () => {
  test('calls the onClick function when the component is clicked', () => {
    const onClick = jest.fn()
    const { getByRole } = render(<Ellipsis onClick={onClick} />)

    const component = getByRole('button')
    fireEvent.click(component)

    expect(onClick).toHaveBeenCalled()
  })
  test('calls the handleHoverChange function when the component is hovered', () => {
    const handleHoverChange = jest.fn()
    const { getByRole } = render(
      <Ellipsis handleHoverChange={handleHoverChange} />
    )

    const component = getByRole('button')
    fireEvent.mouseEnter(component)

    expect(handleHoverChange).toHaveBeenCalledWith(true)

    fireEvent.mouseLeave(component)

    expect(handleHoverChange).toHaveBeenCalledWith(false)
  })
})
