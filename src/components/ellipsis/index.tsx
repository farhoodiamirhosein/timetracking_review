import { useState } from 'react'
import Icon from '../uikit/icon'
import classNames from 'classnames'
import useHover from '@/hooks/useHover'
import colors from '@/styles/theme/colors'

export interface EllipsisProps {
  handleHoverChange?: (value: boolean) => void
  className?: string
  onClick?: () => void
}

const Ellipsis = ({ handleHoverChange, className, onClick }: EllipsisProps) => {
  const { isHovered, hoverEvents } = useHover((value) => {
    if (handleHoverChange) handleHoverChange(value)
  })

  const handleClick = () => {
    if (onClick) onClick()
  }

  return (
    <button onClick={handleClick} {...hoverEvents}>
      <Icon
        name="ellipsis"
        size={20}
        className={classNames(
          'cursor-pointer transition duration-200',
          isHovered ? 'scale-110' : 'scale-100',
          className
        )}
        color={isHovered ? 'white' : colors.primary.pale.blue}
      />
    </button>
  )
}

export default Ellipsis
