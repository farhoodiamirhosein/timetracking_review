import { render } from '@testing-library/react'
import Avatar from './index'

describe('Avatar component', () => {
  it('renders an image with the correct props', async () => {
    const src = '/image-jeremy.png'
    const size = 64
    const alt = 'Test avatar'
    const { getByRole } = render(<Avatar src={src} size={size} alt={alt} />)
    const image = getByRole('img') as HTMLImageElement
    expect(image).toBeInTheDocument()
    expect(image.width).toEqual(size)
    expect(image.height).toEqual(size)
    expect(image.alt).toEqual(alt)
  })
})
