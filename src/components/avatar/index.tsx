import classNames from 'classnames'
import Image from 'next/image'

export interface AvatarPropTypes {
  src: string
  size?: number
  alt?: string
  className?: string
}

const Avatar = ({
  src,
  alt = 'avatar',
  size = 64,
  className,
}: AvatarPropTypes) => {
  return (
    <div
      className={classNames(
        `rounded-full overflow-hidden relative border-[3px] flex justify-center items-center border-white w-max h-max`,
        className
      )}
    >
      <Image width={size} height={size} src={src} alt={alt} />
    </div>
  )
}

export default Avatar
