import {
  TimeFrameTypeProps,
  TimeFrameTitleProps,
} from '@/api/timeTracking/requestTypes'
import Ellipsis from '@/components/ellipsis'
import Icon from '@/components/uikit/icon'
import Heading from '@/components/uikit/typography/heading'
import useHover from '@/hooks/useHover'
import getTimeTrackingColor from '@/utils/getTimeTrackingColor'
import getTimeTrackingIconName from '@/utils/getTimeTrackingIconProps'
import classNames from 'classnames'
import { useMemo, useState } from 'react'
import { timeFramePropType } from '../sidebar/typeSwitcher'
import Paragraph from '@/components/uikit/typography/paragraph'
import getTimeTrackingPreviousLabel from '@/utils/getTimeTrackingPreviousLabel'

export interface TimeTrackingCardProps {
  title: TimeFrameTitleProps
  timeFrames: TimeFrameTypeProps
  onClick?: () => void
  type: timeFramePropType
}

const TimeTrackingCard = ({
  title,
  timeFrames,
  onClick,
  type,
}: TimeTrackingCardProps) => {
  const { isHovered, hoverEvents } = useHover()
  const [isChildHovered, setIsChildHovered] = useState(false)

  const backgroundColor = useMemo(() => getTimeTrackingColor(title), [title])
  const previousLabel = useMemo(
    () => getTimeTrackingPreviousLabel(type),
    [type]
  )

  const handleClick = () => {
    if (onClick) onClick()
  }

  const { current, previous } = timeFrames[type?.value]
  return (
    <section
      className={`pt-[44px] relative sm:h-full cursor-pointer`}
      {...hoverEvents}
    >
      <div
        style={{ backgroundColor }}
        className="top-0 bottom-[50%] absolute right-[1px] left-[1px] sm:right-0 sm:left-0"
      />
      <header className="absolute right-4 -top-[10px]">
        <Icon {...getTimeTrackingIconName(title)} />
      </header>
      <div
        className={classNames(
          'rounded-xl p-7 flex flex-col gap-1 justify-between bg-primary-dark-blue-100 z-20 relative h-full min-h-full transition duration-200',
          !isChildHovered && isHovered ? 'bg-primary-light-blue-100' : ''
        )}
        onClick={handleClick}
      >
        <div className="flex items-center justify-between">
          <Heading type="h3" className="text-white font-medium text-lg">
            {title}
          </Heading>
          <Ellipsis handleHoverChange={setIsChildHovered} />
        </div>

        <div className="flex gap-1 lg:gap-2 sm:flex-col justify-between">
          <Paragraph className="text-2xl sm:text-2xl md:text-3xl lg:text-4xl xl:text-5xl text-white font-light">
            {`${current}hrs`}
          </Paragraph>
          <Paragraph className="text-md text-primary-pale-blue font-light">
            {`${previousLabel} - ${previous}hrs`}
          </Paragraph>
        </div>
      </div>
    </section>
  )
}

export default TimeTrackingCard
