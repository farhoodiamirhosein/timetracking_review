import useTimeTracking from '@/hooks/useTimeTracking'
import SideBar from './sidebar'
import TimeTrackingCard from './timeTrackingCard'

const TimeTrackingComponent = () => {
  const { data, type } = useTimeTracking()

  return (
    <div className="grid sm:grid-cols-3 lg:grid-cols-4 grid-cols-1 gap-6">
      <div className="sm:h-full sm:row-span-3 lg:row-span-2 min-h-32 w-full min-w-[180px] lg:min-w-[180px] xl:min-w-[220px] rounded-xl overflow-hidden">
        <SideBar />
      </div>
      {data?.map((value) => (
        <div
          key={value.title}
          className="w-full min-w-[180px] lg:min-w-[180px] xl:min-w-[220px] rounded-t-xl overflow-hidden"
        >
          <TimeTrackingCard
            title={value.title}
            timeFrames={value.timeframes}
            type={type}
          />
        </div>
      ))}
    </div>
  )
}

export default TimeTrackingComponent
