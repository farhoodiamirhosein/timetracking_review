import Avatar from '@/components/avatar'
import Heading from '@/components/uikit/typography/heading'
import Paragraph from '@/components/uikit/typography/paragraph'
const UserFakeData = {
  img: '/images/image-jeremy.png',
  name: 'Jeremy Robson',
}

const Profile = () => {
  return (
    <div className="bg-primary-blue p-7 sm:pb-20 rounded-xl">
      <div className="flex sm:flex-col flex-row gap-4">
        <Avatar src={UserFakeData.img} />
        <div className="sm:pt-8 flex justify-center flex-col">
          <Paragraph className="font-medium sm:font-normal text-sm text-primary-pale-blue opacity-70">
            Report for
          </Paragraph>
          <Heading
            type="h2"
            className="text-white text-xl sm:text-3xl md:text-4xl sm:py-1 font-light"
          >
            {UserFakeData.name}
          </Heading>
        </div>
      </div>
    </div>
  )
}

export default Profile
