import useTimeTracking from '@/hooks/useTimeTracking'
import classNames from 'classnames'

export type timeFramePropType = {
  label: 'Daily' | 'Monthly' | 'Weekly'
  value: 'daily' | 'monthly' | 'weekly'
}

export const showTypes: timeFramePropType[] = [
  { label: 'Daily', value: 'daily' },
  { label: 'Weekly', value: 'weekly' },
  { label: 'Monthly', value: 'monthly' },
]

const TypeSwitcher = () => {
  const { type, setTimeTrackingType } = useTimeTracking()

  return (
    <ul className="p-7 gap-3 flex sm:flex-col sm:justify-normal justify-between">
      {showTypes?.map((showType) => (
        <li
          className={classNames(
            showType.value === type.value
              ? 'text-white'
              : 'text-primary-desaturated-blue',
            'cursor-pointer font-normal sm:font-normal w-max text-md sm:text-lg hover:text-white transition duration-300'
          )}
          key={showType.value}
          onClick={() => setTimeTrackingType && setTimeTrackingType(showType)}
        >
          {showType.label}
        </li>
      ))}
    </ul>
  )
}

export default TypeSwitcher
