import React from 'react'
import Profile from './profile'
import TypeSwitcher from './typeSwitcher'

const SideBar = () => {
  return (
    <aside className="bg-primary-dark-blue-100 sm:h-full">
      <Profile />
      <TypeSwitcher />
    </aside>
  )
}

export default SideBar
