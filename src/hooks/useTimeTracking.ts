import { TimeTrakingContext } from '@/contexts/timeTrackerContext'
import { useContext } from 'react'

const useTimeTracking = () => {
  const context = useContext(TimeTrakingContext)

  if (context === undefined) {
    throw new Error('useTheme must be used within a ThemeProvider')
  }
  return context
}

export default useTimeTracking
