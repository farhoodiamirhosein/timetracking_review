import { useState } from 'react'

const useHover = (
  onHoverChange?: (value: boolean) => void,
  defaultValue?: boolean
) => {
  const [isHovered, setIsHovered] = useState(defaultValue || false)
  const onMouseLeave = () => {
    setIsHovered(false)
    if (onHoverChange) onHoverChange(false)
  }
  const onMouseEnter = () => {
    setIsHovered(true)
    if (onHoverChange) onHoverChange(true)
  }

  return { hoverEvents: { onMouseEnter, onMouseLeave }, isHovered }
}

export default useHover
