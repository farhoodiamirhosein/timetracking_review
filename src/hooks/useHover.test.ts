import { renderHook, act } from '@testing-library/react'
import useHover from './useHover'

test('should return default value when no arguments are passed', () => {
  const { result } = renderHook(() => useHover())

  expect(result.current.isHovered).toBe(false)
})

test('should return default value when defaultValue is passed', () => {
  const { result } = renderHook(() => useHover(undefined, true))

  expect(result.current.isHovered).toBe(true)
})

test('should call onHoverChange with true when mouse enters', () => {
  const onHoverChange = jest.fn()
  const { result } = renderHook(() => useHover(onHoverChange))

  act(() => {
    result.current.hoverEvents.onMouseEnter()
  })

  expect(result.current.isHovered).toBe(true)
  expect(onHoverChange).toHaveBeenCalledWith(true)
})

test('should call onHoverChange with false when mouse leaves', () => {
  const onHoverChange = jest.fn()
  const { result } = renderHook(() => useHover(onHoverChange, true))

  act(() => {
    result.current.hoverEvents.onMouseLeave()
  })

  expect(result.current.isHovered).toBe(false)
  expect(onHoverChange).toHaveBeenCalledWith(false)
})
