import { renderHook } from '@testing-library/react'
import TimeTrackingProvider from '@/contexts/timeTrackerContext'
import useTimeTracking from './useTimeTracking'
import React, { ReactElement, ReactNode } from 'react'
import { showTypes } from '@/components/pages/main/sidebar/typeSwitcher'

describe('useTimeTracking', () => {
  it('should throw an error if used outside of TimeTrakingContext.Provider', () => {
    try {
      const myFunction = () =>
        renderHook(() => useTimeTracking(), {
          wrapper: ({ children }: any) => <div>{children}</div>,
        })

      expect(myFunction).toThrowError(
        'useTheme must be used within a ThemeProvider'
      )
    } catch (e) {}
  })

  it('should return the value from TimeTrakingContext.Provider', () => {
    const contextValue = {
      type: showTypes[0],
      data: undefined,
    }
    const { result } = renderHook(() => useTimeTracking(), {
      wrapper: ({ children }: any) => (
        <TimeTrackingProvider initialTimeTrakings={contextValue}>
          {children}
        </TimeTrackingProvider>
      ),
    })
    expect({ data: result.current.data, type: result.current.type }).toEqual(
      contextValue
    )
  })
})
