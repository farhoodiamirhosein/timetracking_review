import { ReactNode } from 'react'

const TrackerWrapper = ({ children }: { children: ReactNode }) => {
  return (
    <div className="z-10 bg-primary-dark-blue-200 w-full max-w-full 2xl:w-auto sm:max-w-container lg:px-24 px-6 md:px-12 xl:px-44 sm:py-60 py-20 max-w-auto flex justify-center items-center sm:max-h-screen lg:max-h-auto">
      {children}
    </div>
  )
}

export default TrackerWrapper
