import { ReactNode } from 'react'
import Shapes from './shapes'

const BackgroundWrapper = ({ children }: { children: ReactNode }) => {
  return (
    <div
      className={`flex min-h-screen flex-col items-center justify-center relative bg-[#f0f0f0]`}
    >
      <Shapes />
      {children}
    </div>
  )
}

export default BackgroundWrapper
