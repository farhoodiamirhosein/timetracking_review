import { TimeFrameTitleProps } from '@/api/timeTracking/requestTypes'
import colors from '@/styles/theme/colors'

const getTimeTrackingColor = (title: TimeFrameTitleProps) => {
  switch (title) {
    case 'Work':
      return colors.primary.light.red[100]
    case 'Play':
      return colors.primary.soft.blue
    case 'Study':
      return colors.primary.light.red[200]
    case 'Exercise':
      return colors.primary.lime.green
    case 'Social':
      return colors.primary.violet
    default:
      return colors.primary.soft.orange
  }
}

export default getTimeTrackingColor
