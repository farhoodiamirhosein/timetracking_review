import getTimeTrackingColor from './index'
import colors from '@/styles/theme/colors'
import { TimeFrameTitleProps } from '@/api/timeTracking/requestTypes'

describe('getTimeTrackingColor', () => {
  it('returns the correct color for the provided title', () => {
    const workTitle: TimeFrameTitleProps = 'Work'
    const playTitle: TimeFrameTitleProps = 'Play'
    const studyTitle: TimeFrameTitleProps = 'Study'
    const exerciseTitle: TimeFrameTitleProps = 'Exercise'
    const socialTitle: TimeFrameTitleProps = 'Social'
    const invalidTitle: any = 'Invalid'

    expect(getTimeTrackingColor(workTitle)).toEqual(
      colors.primary.light.red[100]
    )
    expect(getTimeTrackingColor(playTitle)).toEqual(colors.primary.soft.blue)
    expect(getTimeTrackingColor(studyTitle)).toEqual(
      colors.primary.light.red[200]
    )
    expect(getTimeTrackingColor(exerciseTitle)).toEqual(
      colors.primary.lime.green
    )
    expect(getTimeTrackingColor(socialTitle)).toEqual(colors.primary.violet)
    expect(getTimeTrackingColor(invalidTitle)).toEqual(
      colors.primary.soft.orange
    )
  })
})
