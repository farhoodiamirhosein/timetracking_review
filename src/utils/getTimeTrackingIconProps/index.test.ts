import getTimeTrackingIconName from './index'

describe('getTimeTrackingIconName function', () => {
  it('returns correct icon name and size for valid time frame titles', () => {
    expect(getTimeTrackingIconName('Work')).toEqual({ name: 'work', size: 79 })
    expect(getTimeTrackingIconName('Play')).toEqual({ name: 'play', size: 79 })
    expect(getTimeTrackingIconName('Study')).toEqual({
      name: 'study',
      size: 79,
    })
    expect(getTimeTrackingIconName('Exercise')).toEqual({
      name: 'exercise',
      size: 75,
    })
    expect(getTimeTrackingIconName('Social')).toEqual({
      name: 'social',
      size: 90,
    })
  })

  it('returns default icon name and size for invalid time frame titles', () => {
    expect(getTimeTrackingIconName('InvalidTitle' as any)).toEqual({
      name: 'self-care',
      size: 67,
    })
  })
})
