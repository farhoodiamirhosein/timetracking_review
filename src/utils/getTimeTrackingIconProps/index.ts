import { TimeFrameTitleProps } from '@/api/timeTracking/requestTypes'

const getTimeTrackingIconName = (title: TimeFrameTitleProps) => {
  switch (title) {
    case 'Work':
      return { name: 'work', size: 79 }
    case 'Play':
      return { name: 'play', size: 79 }
    case 'Study':
      return { name: 'study', size: 79 }
    case 'Exercise':
      return { name: 'exercise', size: 75 }
    case 'Social':
      return { name: 'social', size: 90 }
    default:
      return { name: 'self-care', size: 67 }
  }
}

export default getTimeTrackingIconName
