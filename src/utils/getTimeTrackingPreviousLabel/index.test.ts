import getTimeTrackingPreviousLabel from './index'
import { timeFramePropType } from '@/components/pages/main/sidebar/typeSwitcher'

describe('getTimeTrackingPreviousLabel', () => {
  it('returns the correct label for monthly', () => {
    const type: timeFramePropType = { value: 'monthly', label: 'Monthly' }
    const result = getTimeTrackingPreviousLabel(type)
    expect(result).toBe('Last Month')
  })

  it('returns the correct label for daily', () => {
    const type: timeFramePropType = { value: 'daily', label: 'Daily' }
    const result = getTimeTrackingPreviousLabel(type)
    expect(result).toBe('Yesterday')
  })

  it('returns the correct label for weekly', () => {
    const type: timeFramePropType = { value: 'weekly', label: 'Weekly' }
    const result = getTimeTrackingPreviousLabel(type)
    expect(result).toBe('Last Week')
  })
})
