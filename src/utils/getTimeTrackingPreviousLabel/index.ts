import { timeFramePropType } from '@/components/pages/main/sidebar/typeSwitcher'

const labels = {
  monthly: 'Last Month',
  daily: 'Yesterday',
  weekly: 'Last Week',
}

const getTimeTrackingPreviousLabel = (type: timeFramePropType) => {
  return labels[type.value]
}

export default getTimeTrackingPreviousLabel
