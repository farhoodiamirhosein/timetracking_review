const colors = {
  primary: {
    dark: {
      blue: {
        100: 'hsl(235, 46%, 20%)',
        200: 'hsl(226, 43%, 10%)',
      },
    },
    light: {
      red: {
        100: 'hsl(15, 100%, 70%)',
        200: 'hsl(348, 100%, 68%)',
      },
      blue: {
        100: '#34397b',
      },
    },
    soft: {
      blue: 'hsl(195, 74%, 62%)',
      orange: 'hsl(43, 84%, 65%)',
    },
    lime: {
      green: 'hsl(145, 58%, 55%)',
    },
    desaturated: {
      blue: 'hsl(235, 45%, 61%)',
    },
    pale: {
      blue: 'hsl(236, 100%, 87%)',
    },
    violet: 'hsl(264, 64%, 52%)',
    blue: 'hsl(246, 80%, 60%)',
  },
}

export default colors
