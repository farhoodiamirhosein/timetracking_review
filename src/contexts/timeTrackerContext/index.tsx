import { GetTimeTrackingResponseTypeProps } from '@/api/timeTracking/requestTypes'
import { timeFramePropType } from '@/components/pages/main/sidebar/typeSwitcher'
import { ReactNode, useState, createContext } from 'react'

export type TimeTrakingsProps = {
  data?: GetTimeTrackingResponseTypeProps
  type: timeFramePropType
}

export type TimeTrackingContextProps = {
  data?: GetTimeTrackingResponseTypeProps
  type?: timeFramePropType
  setTimeTrakingData: (value: GetTimeTrackingResponseTypeProps) => void
  setTimeTrackingType: (value: timeFramePropType) => void
}

export type TimeTrackingProviderProps = {
  children: ReactNode
  initialTimeTrakings: TimeTrakingsProps
}

const initialState = {
  setTimeTrakingData: () => {},
  setTimeTrackingType: () => {},
}

export const TimeTrakingContext =
  createContext<TimeTrackingContextProps>(initialState)

export const TimeTrackingProvider = ({
  children,
  initialTimeTrakings,
}: TimeTrackingProviderProps) => {
  const [data, setData] = useState(initialTimeTrakings?.data)
  const [type, setType] = useState(initialTimeTrakings?.type)

  const setTimeTrakingData = (value: GetTimeTrackingResponseTypeProps) => {
    setData(value)
  }

  const setTimeTrackingType = (value: timeFramePropType) => {
    setType(value)
  }

  return (
    <TimeTrakingContext.Provider
      value={{
        data,
        type,
        setTimeTrakingData,
        setTimeTrackingType,
      }}
    >
      {children}
    </TimeTrakingContext.Provider>
  )
}

export default TimeTrackingProvider
