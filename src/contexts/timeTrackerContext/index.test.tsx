import { fireEvent, render, screen } from '@testing-library/react'
import TimeTrackingProvider, { TimeTrakingContext } from './index'
import { useContext } from 'react'
import { timeFramePropType } from '@/components/pages/main/sidebar/typeSwitcher'
import { GetTimeTrackingResponseTypeProps } from '@/api/timeTracking/requestTypes'

describe('TimeTracking context', () => {
  it('sets and changes data and type correctly', () => {
    const initialType: timeFramePropType = {
      label: 'Monthly',
      value: 'monthly',
    }
    const newType: timeFramePropType = { label: 'Weekly', value: 'weekly' }

    const TestComponent = () => {
      const { type, setTimeTrackingType, setTimeTrakingData } =
        useContext(TimeTrakingContext)
      return (
        <>
          <div data-testid="type">{type?.label}</div>
          <button
            data-testid="channge-tracking-type"
            onClick={() => setTimeTrackingType(newType)}
          >
            Change Type
          </button>
        </>
      )
    }

    render(
      <TimeTrackingProvider initialTimeTrakings={{ type: initialType }}>
        <TestComponent />
      </TimeTrackingProvider>
    )

    expect(screen.getByTestId('type')).toHaveTextContent('Monthly')
    fireEvent.click(screen.getByTestId('channge-tracking-type'))
    expect(screen.getByTestId('type')).toHaveTextContent('Weekly')
  })
})
