/** @type {import('tailwindcss').Config} */
const colors = require('./src/styles/theme/colors')
const sizes = require('./src/styles/theme/sizes')
const withImages = require('next-images')

module.exports = withImages({
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    './src/hocs/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      colors,
      fontFamily: {
        rubik: ['var(--rubik-font)'],
      },
      screens: {
        '2xl': `${sizes.max_width}px`,
      },
      maxWidth: {
        container: `${sizes.max_width}px`,
      },
    },
    plugins: [],
  },
})
